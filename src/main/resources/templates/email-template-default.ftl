<!DOCTYPE html>
<html>

<head>
    <title> </title>
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
</head>

<body>
<div style=\"box-sizing:border-box\">
    <table
            style="box-sizing:border-box;border-collapse:collapse;border-spacing:0;max-width:100%;margin-bottom:18px;width:660px;margin:0 auto;background-color:#f3f4f7;font-family:'Microsoft JhengHei','\005fae\008edf\006b63\009ed1\009ad4','Microsoft YaHei','\005fae\008f6f\0096c5\009ed1',Arial,sans-serif"
            width="660" bgcolor="#F3F4F7">
        <thead style="box-sizing:border-box">
        <tr style="box-sizing:border-box">
            <th style="box-sizing:border-box;text-align:left;margin-bottom:0;padding-left:0;list-style:none;line-height:1.42857;vertical-align:bottom;border-bottom:2px solid #ddd;border-top:0;border:none;background:#fff;color:#848795;padding:18px 27px;width:50%"
                align="left" valign="bottom"><img
                    style="box-sizing:border-box;border:0;vertical-align:middle;width:auto;height:auto;max-width:200px;max-height:80px">
            </th>
            <th style="box-sizing:border-box;text-align:right;margin-bottom:0;padding-left:0;list-style:none;line-height:1.42857;vertical-align:bottom;border-bottom:2px solid #ddd;border-top:0;border:none;background:#fff;color:#848795;padding:18px 27px;width:50%"
                align="right" valign="bottom"><span
                    style="box-sizing:border-box;vertical-align:top;margin-left:5px;font-size:16px;font-weight:300;word-break:break-all;word-wrap:break-word">Java
                            Code Base Studio</span></th>
        </tr>
        <tr style="box-sizing:border-box">
            <th colspan="2"
                style="box-sizing:border-box;text-align:left;line-height:1.42857;border-top:1px solid #ddd;vertical-align:bottom;border-bottom:2px solid #ddd;border:none;padding:20px 27px 18px;width:660px"
                align="left" valign="bottom">
                <h5
                        style="box-sizing:border-box;font-family:inherit;color:inherit;margin-top:0px;margin-bottom:5px;line-height:15px;font-weight:700;font-size:13px;word-break:break-all;word-wrap:break-word">
                    ${subject}</h5>
                <h5
                        style="box-sizing:border-box;font-family:inherit;color:inherit;margin-top:0px;margin-bottom:5px;font-weight:300;font-size:23px;word-break:break-all;word-wrap:break-word;line-height:31px">
                    ${sentDate} 郵件測試</h5>
            </th>
        </tr>
        </thead>
        <tbody style="box-sizing:border-box">
        <tr style="box-sizing:border-box">
            <td colspan="2"
                style="box-sizing:border-box;line-height:1.42857;vertical-align:top;border-top:1px solid #ddd;border:none;padding:0px 27px"
                valign="top">
                <div
                        style="box-sizing:border-box;border-color:#ebedf2;border-style:solid;border-width:1px;background-color:#ffffff;border-left-width:2px;border-left-color:#4a90e2">
                    <div
                            style="box-sizing:border-box;border-top-width:12px;border-bottom-width:12px;border-left-width:17px;border-right-width:17px;border-color:#ffffff;border-style:solid">
                        <p
                                style="box-sizing:border-box;margin:0;font-size:13px;font-weight:300;display:block;margin-top:3px;word-break:break-all;word-wrap:break-word;line-height:18px">
                            ${content}
                        </p>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
        <tfoot style="box-sizing:border-box">
        <tr style="box-sizing:border-box">
            <td colspan="2"
                style="box-sizing:border-box;line-height:1.42857;vertical-align:top;border-top:1px solid #ddd;border:none;padding:20px 27px 18px;width:660px;text-align:center;color:#090f27"
                width="660" valign="top" align="center">
                <a style="box-sizing:border-box;text-decoration:none;margin-bottom:0;font-weight:400;text-align:center;vertical-align:middle;background-image:none;border:1px solid transparent;white-space:nowrap;font-size:13px;color:#fff;display:block;line-height:15px;border-radius:0px;padding:0px;border-color:#4a90e2;border-style:solid;background-color:#4a90e2;word-break:break-all;word-wrap:break-word;border-left-width:40px;border-right-width:40px;border-top-width:8px;border-bottom-width:8px;width:100%;outline:none"
                   href="http://www.google.com.tw/" target="_blank">點此前往Google</a>
                <p
                        style="box-sizing:border-box;margin:0 0 9px;line-height:15px;margin-bottom:0px;word-break:break-all;word-wrap:break-word;word-break:break-word;margin-top:15px">
                    此電郵是來自於 Java Code Base 的電郵API</p>
                <h5
                        style="box-sizing:border-box;font-family:inherit;font-weight:400;color:inherit;line-height:15px;font-size:12px;margin-bottom:9px;margin-top:35px;word-break:break-all;word-wrap:break-word;word-break:break-word">
                    本內容經 Java Code Base 的電子服務提供。以上內容、商標和標記屬 Java Code
                    Base、<wbr>相關機構或版權擁有人所有，並保留一切權利。<wbr>使用者提供的任何內容由使用者自行負責，Java Code Base 不會對該等內容、<wbr>版權許可或由此引起的任何損害 / 損失承擔責任。
                </h5>
                <h5
                        style="box-sizing:border-box;font-family:inherit;font-weight:400;color:inherit;margin-top:0px;line-height:15px;font-size:12px;margin-bottom:0;word-break:break-all;word-wrap:break-word;word-break:break-word">
                    Java Code Base 2021 版權所有</h5>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
</body>

</html>