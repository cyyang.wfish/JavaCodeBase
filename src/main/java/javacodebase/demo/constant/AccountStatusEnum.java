package javacodebase.demo.constant;

public enum AccountStatusEnum {

    ACTIVE("A"),
    DEACTIVATE("D"),
    DELETED("X");

    public String value;

    AccountStatusEnum(String value) {
        this.value = value;
    }

}
