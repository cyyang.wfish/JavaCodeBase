package javacodebase.demo.constant;

public class MessageKeys {

    // API Return Codes
    public static final String SUCCESS = "50000";
    public static final String ALREADY_REGISTERED = "R00001";
    public static final String EMAIL_NOT_REGISTERED = "R00002";
    public static final String NORMAL_LOGIN_FAILED = "R00003";
    public static final String ACTIVATE_SUCCESSFUL = "R00004";
    public static final String ACCOUNT_ALREADY_ACTIVATED = "R00005";
    public static final String ACTIVATE_FAILED = "R00006";
    public static final String RESET_PWD_CODE_NOT_VALID = "R00007";

}
