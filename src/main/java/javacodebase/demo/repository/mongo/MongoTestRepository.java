package javacodebase.demo.repository.mongo;

import javacodebase.demo.entity.mongo.MongoTest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoTestRepository extends MongoRepository<MongoTest, String> {
}
