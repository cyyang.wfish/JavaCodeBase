package javacodebase.demo.repository.mysql;

import javacodebase.demo.entity.mysql.MySqlTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MySqlTestRepository extends JpaRepository<MySqlTest, String> {
}
