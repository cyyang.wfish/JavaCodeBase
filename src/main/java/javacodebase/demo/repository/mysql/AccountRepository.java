package javacodebase.demo.repository.mysql;

import javacodebase.demo.entity.mysql.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, String> {

    List<Account> findByEmail(String email);
    List<Account> findByActiveCode(String activeCode);

}
