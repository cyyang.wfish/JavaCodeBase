package javacodebase.demo.repository.redis;

import javacodebase.demo.entity.redis.RedisTest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RedisTestRepository extends CrudRepository<RedisTest, String> {
}
