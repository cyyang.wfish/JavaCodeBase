package javacodebase.demo;

import javacodebase.demo.repository.mongo.MongoTestRepository;
import javacodebase.demo.repository.mysql.MySqlTestRepository;
import javacodebase.demo.repository.redis.RedisTestRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = MongoTestRepository.class)
@EnableJpaRepositories(basePackageClasses = MySqlTestRepository.class)
@EnableRedisRepositories(basePackageClasses = RedisTestRepository.class)
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
        