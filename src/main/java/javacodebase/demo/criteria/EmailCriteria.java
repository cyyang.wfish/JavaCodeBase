package javacodebase.demo.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailCriteria {

    private String subject;

    @NonNull
    private String sender;

    private String senderName;

    @NonNull
    private String[] recipient;

    private String[] ccList;
    private String content;
    private String template;

    // TODO: for account active temp use, need to change as flexible email items
    private String activateLink;
    private String resetPwdLink;

}
