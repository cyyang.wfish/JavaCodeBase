package javacodebase.demo.criteria;

import lombok.Data;

@Data
public class AccountCriteria {

    private String nickname;
    private String name;
    private String email;
    private String phone;
    private String password;
    private String resetPwdCode;

}
