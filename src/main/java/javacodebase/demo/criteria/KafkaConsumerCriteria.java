package javacodebase.demo.criteria;

import lombok.Data;

@Data
public class KafkaConsumerCriteria {

    private String topic;
}
