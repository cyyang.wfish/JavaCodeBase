package javacodebase.demo.criteria;

import javacodebase.demo.kafka.KafkaRecord;
import lombok.Data;

import java.util.List;

@Data
public class KafkaProducerCriteria {

    private List<KafkaRecord> records;

}
