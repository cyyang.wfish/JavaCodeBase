package javacodebase.demo.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration
public class MongoConfiguration {

    @Value("${spring.data.mongodb.database}")
    private String dbName;

    @Value("${spring.data.mongodb.servers}")
    private String dbServers;

    @Value("${spring.data.mongodb.username}")
    private String dbUsername;

    @Value("${spring.data.mongodb.password}")
    private String dbPassword;

    @Bean
    public MongoClient mongoClient() {
        if (dbUsername == null || dbUsername.length() < 1) {
            return MongoClients.create(String.format("mongodb://%s/%s", dbServers, dbName));
        } else {
            return MongoClients.create(String.format("mongodb://%s:%s@%s/%s", dbUsername, dbPassword, dbServers, dbName));
        }
    }

    @Bean
    public MongoDatabaseFactory mongoDatabaseFactory() {
        return new SimpleMongoClientDatabaseFactory(mongoClient(), dbName);
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoDatabaseFactory());
    }
}
