package javacodebase.demo.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResult<T> implements Serializable {

    public enum STATUS {
        SUCCESS,
        BAD_REQUEST,
        INTERNAL_ERROR
    }

    private STATUS status;
    private String returnCode;
    private String message;
    public T data;

    public BaseResult(T data) {
        super();
        this.data = data;
    }

    public BaseResult(Throwable e) {
        super();
        this.message = e.toString();
    }
}
