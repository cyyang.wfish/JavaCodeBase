package javacodebase.demo.aspect;

import javacodebase.demo.result.BaseResult;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.springframework.stereotype.Service;

@Slf4j
@org.aspectj.lang.annotation.Aspect
@Service
public class Aspect {

    @Around("execution(* javacodebase.demo.api.*.*(..))")
    public Object basicAspect(ProceedingJoinPoint joinPoint) throws Throwable {
        String signatureName = joinPoint.getSignature().getName();
        try {
            long startTime = 0;
            startTime = System.currentTimeMillis();
            // TODO: add more pointcut check procedure here
            String costTime = (System.currentTimeMillis() - startTime) + "ms";
            Object object = joinPoint.proceed();
            log.debug("Call API [{}] cost: {}", signatureName, costTime);
            return object;
        } catch (Exception e) {
            log.error("Call API [" + signatureName + "] error[UNKNOWN_ERROR]: ", e.getMessage());
            BaseResult result = new BaseResult(e);
            return result;
        }
    }
}
