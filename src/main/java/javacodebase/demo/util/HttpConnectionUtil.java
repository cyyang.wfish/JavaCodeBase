package javacodebase.demo.util;

import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

/**
 * Use in call backend API or 3rd party API, and same host(include port) share connection pool.
 */
public class HttpConnectionUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpConnectionUtil.class);

    private static final int MAX_CONNECTION_POOL = 50;
    private static final int TIMEOUT_MILLISECONDS = 120000;
    private static final int CONNECTION_REQUEST_TIMEOUT_MILLISECONDS = 120000;

    private static HttpConnectionUtil instance;
    // key is server info, include host and port.
    private Map<String, CloseableHttpClient> mHttpClientMap = new HashMap<String, CloseableHttpClient>();
    private PoolingHttpClientConnectionManager cm;
    private RequestConfig defaultRequestConfig;

    public static HttpConnectionUtil getInstance() {
        if (instance == null) {
            synchronized (HttpConnectionUtil.class) {
                if (instance == null)
                    instance = new HttpConnectionUtil();
            }
        }
        return instance;
    }

    private HttpConnectionUtil() {
        cm = new PoolingHttpClientConnectionManager();
        cm.setDefaultMaxPerRoute(MAX_CONNECTION_POOL);
        cm.setMaxTotal(MAX_CONNECTION_POOL);
        defaultRequestConfig = RequestConfig.custom()
                /* socketTimeout like the description says, is the timeout while waiting for data.
                 * Usually happens when your server is slow. */
                .setSocketTimeout(TIMEOUT_MILLISECONDS)
                /* connectTimeout happens when establishing the connection. For instance while doing the tcp handshake. */
                .setConnectTimeout(TIMEOUT_MILLISECONDS)
                /* connectionRequestTimeout happens when you have a pool of connections and they are all busy,
                 * not allowing the connection manager to give you one connection to make the request. */
                .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT_MILLISECONDS)
                .build();
    }

    private static HttpPost createPostHttp(String uriStr, String jsonObjStr,
                                           String contentType, String accept) throws UnsupportedEncodingException {
        HttpPost postRequest = new HttpPost(uriStr);
        if (Util.isEmpty(uriStr)) {
            return null;
        }
        postRequest.setHeader("Content-Type", contentType);
        if (!Util.isEmpty(accept)) postRequest.setHeader("Accept", accept);
        if (jsonObjStr != null) {
            postRequest.setEntity(new StringEntity(jsonObjStr, "UTF-8"));
        }
        return postRequest;
    }

    private static HttpGet createGetHttp(String uriStr) {
        HttpGet getRequest = new HttpGet(uriStr);
        if (Util.isEmpty(uriStr)) {
            return null;
        }
        return getRequest;
    }

    private static HttpPut createPutHttp(String uriStr, String jsonObjStr,
                                         String contentType, String accept) throws UnsupportedEncodingException {
        HttpPut putRequest = new HttpPut(uriStr);
        if (Util.isEmpty(uriStr)) {
            return null;
        }
        putRequest.setHeader("Content-Type", contentType + "; charset=utf-8");
        if (!Util.isEmpty(accept)) putRequest.setHeader("Accept", accept);
        putRequest.setEntity(new StringEntity(jsonObjStr, "UTF-8"));
        return putRequest;
    }

    /**
     * Connect to target url and get body of response, and support GET and POST currently.
     *
     * @param method      Support GET and POST.
     * @param urlStr      Target url string, include path parameter, query parameter, etc.
     * @param bodyJsonStr Body of request for POST.
     * @param contentType in header.
     * @param accept      Http headers Accept.
     * @return body of response while http status code is 200; otherwise, empty string.
     */
    public String connect(HttpMethod method, String urlStr, String bodyJsonStr, String contentType, String accept)
            throws Exception {
        String result = "";
        HttpRequestBase requestBase;
        if (HttpMethod.GET.equals(method)) {
            requestBase = createGetHttp(urlStr);
        } else if (HttpMethod.POST.equals(method)) {
            requestBase = createPostHttp(urlStr, bodyJsonStr, contentType, accept);
        } else if (HttpMethod.PUT.equals(method) && !Util.isEmpty(bodyJsonStr)) {
            requestBase = createPutHttp(urlStr, bodyJsonStr, contentType, accept);
        } else {
            LOGGER.error("not support method:" + method + ", url:" + urlStr
                    + ", contentType:" + contentType + ", bodyJsonStr:" + bodyJsonStr);
            return result;
        }
        CloseableHttpClient httpClient = getHttpClient(urlStr);
        long begin = System.currentTimeMillis();
        LOGGER.debug("Http begin to connect... " + urlStr);
        CloseableHttpResponse response = httpClient.execute(requestBase);
        try {
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                result = EntityUtils.toString(response.getEntity(), "UTF-8");
            } else if (HttpMethod.PUT.equals(method) && response.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
                // userProfile might return 400. TODO: this is a dirty way.
                result = String.valueOf(response.getStatusLine().getStatusCode());
            } else {
                LOGGER.error("fail to connect to " + urlStr + ", statusCode:" + response.getStatusLine().getStatusCode());
                throw new Exception("http statusCode is not 200" + ", statusCode:" + response.getStatusLine().getStatusCode());
            }
            long end = System.currentTimeMillis();
            LOGGER.debug("Http connect end took:" + (end - begin));
        } finally {
            response.close();
        }
        return result;
    }

    @Override
    public synchronized void finalize() {
        try {
            for (String hostUrl : mHttpClientMap.keySet()) {
                if (mHttpClientMap.get(hostUrl) != null) mHttpClientMap.get(hostUrl).close();
            }
            if (cm != null) cm.shutdown();
        } catch (IOException e) {
            LOGGER.debug(e.getMessage());
        }
    }

    /**
     * Get httpCient by url address, that could map to different connection pool.
     *
     * @param urlStr Target url string, include path parameter, query parameter, etc.
     * @return httpClient.
     */
    private synchronized CloseableHttpClient getHttpClient(String urlStr) throws Exception {
        URL url = new URL(urlStr);
        StringBuilder urlStrBuilder = new StringBuilder();
        urlStrBuilder.append(url.getHost());
        if (url.getPort() != -1) urlStrBuilder.append(":").append(url.getPort());
        CloseableHttpClient httpClient = mHttpClientMap.get(urlStrBuilder.toString());
        if (httpClient == null) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).setConnectionManager(cm).setDefaultRequestConfig(defaultRequestConfig).build();
            mHttpClientMap.put(urlStrBuilder.toString(), httpClient);
        }
        return httpClient;
    }
}


