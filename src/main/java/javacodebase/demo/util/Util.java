package javacodebase.demo.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Utility class.
 */
public final class Util {

    private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);

    /** MilliSecond in a minute. */
    public static final int MILLISECOND_A_SECOND = 1000;

    /**
     * utility class, make the class final and define a private constructor.
     */
    private Util() {
        // not called
    }

    /**
     * Check if the string is empty or null.
     *
     * @param str
     *            The String want to check.
     * @return <code>true</code>: the string is empty or null; <code>false</code>:
     *         the string is not empty or null.
     */
    public static boolean isEmpty(String str) {
        return str == null || str.equals("");
    }

    /**
     * Check if the array is empty or null.
     *
     * @param array
     *            A Object array.
     * @return <code>true</code>: the array is null or length is zero;
     *         <code>false</code>: the array is not null and length > 0.
     */
    public static boolean isEmptyArr(Object[] array) {
        return array == null && array.length == 0;
    }

    /**
     * Check if the string is space only or empty or null.
     *
     * @param str
     *            The String want to check.
     * @return <code>true</code>: the string is space only or empty or null;
     *         <code>false</code>: the string is not null and not space only.
     */
    public static boolean isEmptyOrSpace(String str) {
        return str == null || str.trim().equals("");
    }

    /**
     * Check if the date is weekend(include SATURDAY, SUNDAY).
     *
     * @param date
     *            The checked Date.
     * @return <code>true</code>: the date is weekend; <code>false</code>: the date
     *         is workdays.
     */
    public static boolean isWeekend(Date date) {
        assert (date != null) : "@param cal should not be NULL";
        Calendar cal = date2Calendar(date);
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        }
        return false;
    }

    /**
     * Convert from Date to Calendar.
     *
     * @param date
     *            that want to convert.
     * @return Calendar that coverted by the date.
     */
    public static Calendar date2Calendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Convert from Date to String according to the format.
     *
     * @param inDate
     *            The date that to convert.
     * @param format
     *            the targeted format.
     * @return The String of date.
     */
    public static String date2Str(Date inDate, String format) {
        String result = "";
        final DateFormat sdf = new SimpleDateFormat(format);
        if (inDate != null) {
            result = sdf.format(inDate);
        }
        return result;
    }

    /**
     * Convert from String to Date according to the format.
     *
     * @param inStr
     *            inputed String.
     * @param format
     *            Targeted format.
     * @return The date converted by inStr.
     */
    public static Date str2Date(String inStr, String format) {
        Date parsedDate = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            parsedDate = formatter.parse(inStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsedDate;
    }

    // Sync with WiseMobileAPI
    /**
     * Check if it is numeric.
     *
     * @param str
     *            inputed String.
     * @return <code>true</code>: it is numeric; <code>false</code>: it is not
     *         numeric.
     */
    public static boolean isNumeric(String str) {
        return str != null && str.matches("-?\\d+(\\.\\d+)?"); // match a number with optional '-' and decimal.
    }

    /**
     * Returns another Date object which added day to the given Date object. <br>
     * Note: it doesn't modify any "time" part.
     *
     * @param date
     *            The Date you want to start with.
     * @param day
     *            The number of day you want to added to the given date.
     * @return a Date object that has been added day to the given Date object.
     */
    public static Date addDay(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, day);
        return cal.getTime();
    }

    /**
     * Returns another Date object which added hour to the given Date object. <br>
     *
     * @param date
     *            The Date you want to start with.
     * @param hour
     *            The number of hours you want to added to the given date.
     * @return a Date object that has been added hours to the given Date object.
     */
    public static Date addHour(Date date, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hour);
        return cal.getTime();
    }

    /**
     * Returns another Date object which added minute to the given Date object. <br>
     *
     * @param date
     *            The Date you want to start with.
     * @param min
     *            The number of minutes you want to added to the given date.
     * @return a Date object that has been added minutes to the given Date object.
     */
    public static Date addMin(Date date, int min) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, min);
        return cal.getTime();
    }

    /**
     * Returns the date that trim time.
     *
     * @param date
     *            The Date you want to start with.
     * @return a Date object that has been added day to the given Date object.
     */
    public static Date trimTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Returns Date of current system.
     *
     * @return Date of Today.
     */
    public static Date getToday() {
        return new Date();
    }

    /**
     * Returns current timestamp of current system.
     *
     * @return Timestamp of Today.
     */
    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * Convert from String to Calendar according to the dateFormat.
     *
     * @param str
     *            that convert to Calendar.
     * @param dateFormat
     *            The dateFormat.
     * @return converted Calendar.
     * @throws ParseException
     *             - if the beginning of the specified string cannot be parsed.
     */
    public static Calendar str2Calendar(String str, String dateFormat) throws ParseException {
        SimpleDateFormat curFormater = new SimpleDateFormat(dateFormat);
        Date dateObj = curFormater.parse(str);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateObj);
        return calendar;
    }

    /**
     * Return the full stack trace from exception.
     *
     * @param e Exception
     * @return The String
     */
    public static String getFullStacktace(Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

    public static String getMD5(String str) {
        String md5Str = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            md5Str = new BigInteger(1, md.digest()).toString(16);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return md5Str;
    }

    public static byte[] encodeToBytes(String source) {
        byte[] bytes = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(source.getBytes("UTF-8"));
            bytes = md.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    public static String encodeTohex(String source) {
        byte[] bytes = encodeToBytes(source);
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xff & bytes[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static boolean validate(String unknown, String okHex) {
        return okHex.equals(encodeTohex(unknown));
    }

    public static String getRandomString(int length) {

        byte[] bytearray = new byte[256];
        new Random().nextBytes(bytearray);
        String text = new String(bytearray, Charset.forName("UTF-8"));
        StringBuffer buffer = new StringBuffer();

        //remove all spacial char
        String theAlphaNumericS = text.replaceAll("[^A-Z0-9]", "");

        //random selection
        for (int m = 0; m < theAlphaNumericS.length(); m++) {
            if (Character.isLetter(theAlphaNumericS.charAt(m)) && (length > 0)
                    || Character.isDigit(theAlphaNumericS.charAt(m)) && (length > 0)) {
                buffer.append(theAlphaNumericS.charAt(m));
                length--;
            }
        }

        // the resulting string
        return buffer.toString().toLowerCase();
    }

}
