package javacodebase.demo.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KafkaRecord {

    private Integer id;
    private String topic;
    private Long offset;
    private Integer partition;
    private String key;
    private String value;

    public KafkaRecord(String topic, String key, String value) {
        this.topic = topic;
        this.key = key;
        this.value = value;
    }
}

