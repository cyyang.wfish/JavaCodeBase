package javacodebase.demo.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@Service
public class KafkaConsumeListener {

    // for application shutdown elegantly.
    private AtomicBoolean shutdown = new AtomicBoolean(false);
    private CountDownLatch shutdownLatch = new CountDownLatch(0);

    @Autowired
    private ApplicationContext context;

    @Value("${kafka.listener.enabled}")
    private boolean enableKafkaListener;

//    @Autowired
//    @Qualifier("threadPoolExecutor")
//    Executor threadPoolExecutor;

    @PreDestroy
    public void cleanUp() throws Exception {
        shutdown.set(true);
        shutdownLatch.await();
    }

    @KafkaListener(id = "${kafka.topic.javacodebase.consumer.group.id}", topics = "${kafka.topic.javacodebase.demo.source}", containerFactory = "customKafkaListenerContainerFactory")
    public void consumeListener(List<ConsumerRecord<String, String>> records, Acknowledgment ack) throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        try {
            shutdownLatch = new CountDownLatch(1);

            records.stream().forEach(record -> {
                log.info("[Record consumed] Topic: " + record.topic() + " Key: " + record.key() + " Partition: " + record.partition()
                        + " Offset: " + record.offset() + " Value: " + record.value());

                try {
                    // TODO: add some handling you need here
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            });

        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            shutdownLatch.countDown();
            ack.acknowledge();  // commit offset manually.
        }
    }
}

