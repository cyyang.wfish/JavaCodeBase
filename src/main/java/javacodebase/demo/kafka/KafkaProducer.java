package javacodebase.demo.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

@Slf4j
@Service
public class KafkaProducer {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void send(KafkaRecord kafkaRecord) {
        List<KafkaRecord> records = new Vector<KafkaRecord>();
        records.add(kafkaRecord);
        this.send(records);
    }

    public void send(List<KafkaRecord> kafkaRecords) {
        if (kafkaRecords != null) {
            List<ProducerRecord<String, String>> records = kafkaRecords.stream().map(record ->
                    new ProducerRecord<String, String>(record.getTopic(), record.getKey(), record.getValue())
            ).collect(Collectors.toList());

            records.stream().forEach(record -> {
                log.info("[Start sending] key: " + record.key() + " value: " + record.value());
                ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(record);
                future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

                    @Override
                    public void onSuccess(SendResult<String, String> result) {
                        log.info("[Sent successful] key: " + record.key() + " value: " + record.value());
                    }

                    @Override
                    public void onFailure(Throwable ex) {
                        ex.printStackTrace();
                        log.error("[Sent failed] key: " + record.key() + " value: " + record.value());
                        log.error(ex.getMessage());
                    }
                });
            });
            kafkaTemplate.flush();
        }
    }
}
