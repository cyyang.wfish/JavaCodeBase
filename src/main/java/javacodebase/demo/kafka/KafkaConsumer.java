package javacodebase.demo.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class KafkaConsumer {

    @Value("${spring.kafka.consumer.auto-offset-reset}")
    private String offsetType;

    public List<KafkaRecord> consume(String brokers, String groupId, String topic) {

        // configure the consumer
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", brokers);
        properties.setProperty("group.id", groupId);
        properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("auto.offset.reset", offsetType);

        // specify the protocol for Domain Joined clusters
        // properties.setProperty(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_PLAINTEXT");

        org.apache.kafka.clients.consumer.KafkaConsumer<String, String> consumer = new org.apache.kafka.clients.consumer.KafkaConsumer<>(properties);
        consumer.subscribe(Arrays.asList(topic));

        ConsumerRecords<String, String> records = new ConsumerRecords<String, String>(Collections.EMPTY_MAP);
        List<KafkaRecord> record = new ArrayList<KafkaRecord>();
        long startTime = System.currentTimeMillis();

        while (true) {
            records = consumer.poll(200);
            if (records.count() != 0) {
                for (ConsumerRecord<String, String> cRecord : records) {
                    record.add(new KafkaRecord(consumer.groupMetadata().generationId(), cRecord.topic(), cRecord.offset(), cRecord.partition(), cRecord.key(), cRecord.value()));
                    log.info("[Record consumed] Topic: " + cRecord.topic() + " Offset: " + cRecord.offset());
                }
                break;
            }

            // poll only 5 secs in API call
            if ((System.currentTimeMillis() - startTime) > 5000) {
                log.info("Kafka consumer polling timeout");
                break;
            }
        }
        consumer.close();
        return record;
    }
}
