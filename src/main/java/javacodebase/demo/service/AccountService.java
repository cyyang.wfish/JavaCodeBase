package javacodebase.demo.service;

import javacodebase.demo.constant.AccountStatusEnum;
import javacodebase.demo.constant.MessageKeys;
import javacodebase.demo.criteria.AccountCriteria;
import javacodebase.demo.criteria.EmailCriteria;
import javacodebase.demo.entity.mysql.Account;
import javacodebase.demo.repository.mysql.AccountRepository;
import javacodebase.demo.result.BaseResult;
import javacodebase.demo.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private EmailService emailService;

    @Value("${hostname}")
    private String hostname;

    @Value("${email.subject}")
    private String subject;

    @Value("${email.sender}")
    private String sender;

    @Value("${email.sender.name}")
    private String senderName;

    @Value("${email.activate.msg}")
    private String activateMsg;

    @Value("${email.reset-pwd.msg}")
    private String resetPwdMsg;

    public BaseResult getAccountInfo(String email) {
        BaseResult result = new BaseResult();
        result.setStatus(BaseResult.STATUS.BAD_REQUEST);
        result.setReturnCode(MessageKeys.EMAIL_NOT_REGISTERED);
        result.setMessage("Account " + email + " is not registered.");

        // check this email registered ot not
        List<Account> accounts = accountRepository.findByEmail(email);
        for (Account acc : accounts) {
            if (acc.getEmail().equals(email)) {
                result.setStatus(BaseResult.STATUS.SUCCESS);
                result.setReturnCode(MessageKeys.ALREADY_REGISTERED);
                result.setMessage("Account " + email + " is already registered.");
                result.setData(acc);
                log.debug("Account " + email + " is already registered.");
                break;
            }
        }
        log.debug("Account " + email + " is not registered.");
        return result;
    }

    public BaseResult signup(AccountCriteria accountCriteria) {
        BaseResult result = new BaseResult();

        // check this email registered ot not
        String email = accountCriteria.getEmail();
        if (accountRepository.findByEmail(email).size() > 0) {
            result.setStatus(BaseResult.STATUS.BAD_REQUEST);
            result.setReturnCode(MessageKeys.ALREADY_REGISTERED);
            result.setMessage("Account " + email + " is already registered.");
            log.debug("Account " + email + " is already registered.");
            return result;
        }

        // add new account into mysql
        Account account = new Account();
        account.setNickname(accountCriteria.getNickname());
        account.setName(accountCriteria.getName());
        account.setEmail(accountCriteria.getEmail());
        account.setPhone(accountCriteria.getPhone());
        account.setPassword(Util.getMD5(accountCriteria.getPassword()));
        account.setStatus(AccountStatusEnum.DEACTIVATE.value);
        account.setActiveCode(Util.encodeTohex(accountCriteria.getEmail()));
        account.setResetCount(0);
        account.setResetCode(Util.encodeTohex(accountCriteria.getEmail() + accountCriteria.getPassword()));
        accountRepository.save(account);

        // TODO: modify email criteria more flexible
        String recipients[] = {email};
        String ccList[] = {};
        String activateLink = hostname + "activate.html?active_code=" + Util.encodeTohex(accountCriteria.getEmail());
        EmailCriteria emailCriteria = new EmailCriteria(subject, sender, senderName, recipients, ccList,
                activateMsg, "activate", activateLink, "");
        emailService.sendEmail(emailCriteria, true, null, null);

        result.setStatus(BaseResult.STATUS.SUCCESS);
        result.setReturnCode(MessageKeys.SUCCESS);
        result.setMessage(MessageKeys.SUCCESS);
        return result;
    }

    public BaseResult login(AccountCriteria accountCriteria) {
        BaseResult result = new BaseResult();
        String email = accountCriteria.getEmail();
        if (!isRegistered(email)) {
            result.setStatus(BaseResult.STATUS.BAD_REQUEST);
            result.setReturnCode(MessageKeys.EMAIL_NOT_REGISTERED);
            result.setMessage("Account " + email + " is not registered.");
            log.debug("Account " + email + " is not registered.");
            return result;
        }

        // check this email registered ot not
        List<Account> accounts = accountRepository.findByEmail(email);
        for (Account acc : accounts) {
            if (acc.getEmail().equals(email)) {
                if (acc.getPassword().equals(accountCriteria.getPassword())) {
                    result.setStatus(BaseResult.STATUS.SUCCESS);
                    result.setReturnCode(MessageKeys.SUCCESS);
                    result.setMessage(MessageKeys.SUCCESS);
                    log.debug("Account " + email + " is login successfully.");
                    return result;
                }
            }
        }

        result.setStatus(BaseResult.STATUS.BAD_REQUEST);
        result.setReturnCode(MessageKeys.NORMAL_LOGIN_FAILED);
        result.setMessage("Account " + email + " is login failed.");
        log.debug("Account " + email + " is login failed.");
        return result;
    }

    public BaseResult activate(String activeCode) {
        BaseResult result = new BaseResult();
        result.setStatus(BaseResult.STATUS.BAD_REQUEST);
        result.setReturnCode(MessageKeys.ACTIVATE_FAILED);
        result.setMessage("Active Code " + activeCode + " is not valid.");

        List<Account> accounts = accountRepository.findByActiveCode(activeCode);
        for (Account acc : accounts) {
            if (acc.getActiveCode().equals(activeCode)) {
                if (acc.getStatus().equals(AccountStatusEnum.ACTIVE.value)) {
                    result.setStatus(BaseResult.STATUS.SUCCESS);
                    result.setReturnCode(MessageKeys.ACCOUNT_ALREADY_ACTIVATED);
                    result.setMessage("Account " + acc.getEmail() + " is already activated.");
                    log.debug("Account " + acc.getEmail() + " is already activated.");
                    return result;
                }
                acc.setStatus(AccountStatusEnum.ACTIVE.value);
                accountRepository.save(acc);
                result.setStatus(BaseResult.STATUS.SUCCESS);
                result.setReturnCode(MessageKeys.ACTIVATE_SUCCESSFUL);
                result.setMessage("Account " + acc.getEmail() + " is activated successfully.");
                log.debug("Account " + acc.getEmail() + " is login successfully.");
                return result;
            }
        }
        return result;
    }

    public BaseResult forgotPwd(String email) {
        BaseResult result = new BaseResult();
        log.debug("Account " + email + " forgot password.");

        if (!isRegistered(email)) {
            result.setStatus(BaseResult.STATUS.BAD_REQUEST);
            result.setReturnCode(MessageKeys.EMAIL_NOT_REGISTERED);
            result.setMessage("Email " + email + " is not valid.");
        }

        List<Account> accounts = accountRepository.findByEmail(email);
        for (Account acc : accounts) {
            if (acc.getEmail().equals(email)) {
                String resetCode = Util.encodeTohex(acc.getEmail() + acc.getPassword());

                String recipients[] = {email};
                String ccList[] = {};
                String resetPwdLink = hostname + "reset-pwd.html?email=" + email + "&reset_code=" + resetCode;
                EmailCriteria emailCriteria = new EmailCriteria(subject, sender, senderName, recipients, ccList,
                        resetPwdMsg, "reset-pwd", "", resetPwdLink);
                emailService.sendEmail(emailCriteria, true, null, null);

                acc.setResetCode(resetCode);
                accountRepository.save(acc);

                result.setStatus(BaseResult.STATUS.SUCCESS);
                result.setReturnCode(MessageKeys.SUCCESS);
                result.setMessage("Sent reset password mail successfully.");
                log.debug("Reset password mail of account " + email + " is sent successfully.");
                break;
            }
        }
        return result;
    }

    public BaseResult resetPwd(AccountCriteria accountCriteria) {
        BaseResult result = new BaseResult();
        String email = accountCriteria.getEmail();
        log.debug("Account " + email + " attempt reset password.");

        if (!isRegistered(email)) {
            result.setStatus(BaseResult.STATUS.BAD_REQUEST);
            result.setReturnCode(MessageKeys.EMAIL_NOT_REGISTERED);
            result.setMessage("Email " + email + " is not valid.");
        }

        List<Account> accounts = accountRepository.findByEmail(email);
        for (Account acc : accounts) {
            if (acc.getEmail().equals(email)) {
                if (!acc.getResetCode().equals(accountCriteria.getResetPwdCode())) {
                    result.setStatus(BaseResult.STATUS.BAD_REQUEST);
                    result.setReturnCode(MessageKeys.RESET_PWD_CODE_NOT_VALID);
                    result.setMessage("Reset Password Code " + accountCriteria.getResetPwdCode() + " is not valid.");
                    return result;
                } else {
                    int resetCount = acc.getResetCount();
                    acc.setPassword(Util.getMD5(accountCriteria.getPassword()));
                    acc.setResetCode(Util.getRandomString(32));  // generate a random string to avoid modify password by using same reset code
                    acc.setResetCount(++resetCount);
                    accountRepository.save(acc);

                    result.setStatus(BaseResult.STATUS.SUCCESS);
                    result.setReturnCode(MessageKeys.SUCCESS);
                    result.setMessage("Password reset successfully.");
                    log.debug("Password of account " + email + " is reset successfully.");
                    break;
                }
            }
        }
        return result;
    }

    private boolean isRegistered(String email) {
        return accountRepository.findByEmail(email).size() > 0 ? true : false;
    }

}
