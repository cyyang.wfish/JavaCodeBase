package javacodebase.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@Slf4j
@Component
@ServerEndpoint(value = "/websocket-test")
public class WebSocketService {

    @OnMessage
    public void onMessage(final Session session, String msg) {
        log.info("Msg: " + msg);
        try {
            session.getBasicRemote().sendText(msg);
        } catch (IOException e) {
            log.debug(e.getMessage());
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        log.info("WebSocket opened: " + session.getId());
    }

    @OnClose
    public void onClose(CloseReason reason) {
        log.info("Closing the websocket-test due to " + reason.getReasonPhrase());
    }

    @OnError
    public void onError(Throwable e) {
        log.debug(e.getMessage());
    }

}

