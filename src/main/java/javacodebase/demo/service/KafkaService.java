package javacodebase.demo.service;

import javacodebase.demo.kafka.KafkaConsumer;
import javacodebase.demo.kafka.KafkaProducer;
import javacodebase.demo.kafka.KafkaRecord;
import javacodebase.demo.result.BaseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class KafkaService {

    @Autowired
    KafkaProducer producer;

    @Autowired
    KafkaConsumer consumer;

    @Value("${spring.kafka.bootstrap-servers}")
    private String brokers;

    @Value("${kafka.topic.javacodebase.consumer.group.id}")
    private String groupId;

    public BaseResult producer(List<KafkaRecord> records) {
        BaseResult result = new BaseResult();
        try {
            producer.send(records);
            result.setStatus(BaseResult.STATUS.SUCCESS);
        } catch (Exception e) {
            log.debug(e.getMessage());
            result.setStatus(BaseResult.STATUS.INTERNAL_ERROR);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    public BaseResult consumer(String topic) {
        BaseResult result = new BaseResult();
        try {
            List<KafkaRecord> records = consumer.consume(brokers, groupId, topic);
            if(records.size() == 0)
                result.setMessage("No records to consume");
            result.setData(records);
            result.setStatus(BaseResult.STATUS.SUCCESS);
        } catch (Exception e) {
            result.setStatus(BaseResult.STATUS.INTERNAL_ERROR);
            result.setMessage(e.getMessage());
        }
        return result;
    }
}
