package javacodebase.demo.service;

import freemarker.template.Configuration;
import javacodebase.demo.criteria.EmailCriteria;
import javacodebase.demo.result.BaseResult;
import javacodebase.demo.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class EmailService {

    private static final String EMAIL_REGULAR_EXP = "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+"
            + "@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$";

    private static final HashMap<String, String> emailTemplateMap = new HashMap<String, String>() {
        {
            put("default", "email-template-default.ftl");
            put("activate", "active-code-mail.ftl");
            put("reset-pwd", "reset-pwd-mail.ftl");
        }
    };

    @Autowired
    private JavaMailSender sender;

    @Autowired
    Configuration fmConfiguration;

    public BaseResult sendEmail(EmailCriteria emailCriteria, boolean isHtml, Map<String, File> inlineImages,
                                Map<String, File> attachments) {
        BaseResult result = new BaseResult();
        try {
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper msgHelper = new MimeMessageHelper(message, true, "UTF-8");
            Map<String, Object> model = new HashMap<String, Object>();

            // subject can be null
            if (!Util.isEmpty(emailCriteria.getSubject())) {
                msgHelper.setSubject(emailCriteria.getSubject());
                model.put("subject", emailCriteria.getSubject());
            }

            // set sent date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            model.put("sentDate", sdf.format(new Date()).toString());

            // sender must be given, but sender name not
            if (!Util.isEmpty(emailCriteria.getSender()) && this.isValidEmailAddress(emailCriteria.getSender())) {
                if (!Util.isEmpty(emailCriteria.getSenderName()))
                    msgHelper.setFrom(emailCriteria.getSender(), emailCriteria.getSenderName());
                else
                    msgHelper.setFrom(emailCriteria.getSender());
            }

            // set recipient
            if (!Util.isEmptyArr(emailCriteria.getRecipient())) {
                for (String addr : emailCriteria.getRecipient()) {
                    this.isValidEmailAddress(addr);
                }
                msgHelper.setTo(emailCriteria.getRecipient());
            }

            // set CCList
            if (!Util.isEmptyArr(emailCriteria.getCcList())) {
                msgHelper.setCc(emailCriteria.getCcList());
            }

            // set content
            if (!Util.isEmpty(emailCriteria.getContent())) {
                model.put("content", emailCriteria.getContent());
                // TODO: for flexible change email template in the future
                String template = "default";
                if (!Util.isEmpty(emailCriteria.getTemplate())) {
                    template = emailCriteria.getTemplate();
                    switch(template){
                        case "activate":
                            model.put("activateLink", emailCriteria.getActivateLink());
                            break;
                        case "reset-pwd":
                            model.put("resetPwdLink", emailCriteria.getResetPwdLink());
                            break;
                        default:
                            // do-nothing, use default test template here
                    }
                }
                String content = this.getContentFromTemplate(emailTemplateMap.get(emailCriteria.getTemplate()), model);
                msgHelper.setText(content, isHtml);
            }

            // set images
            if (inlineImages != null) {
                inlineImages.entrySet().stream().forEach(element -> {
                    try {
                        msgHelper.addInline(element.getKey(), element.getValue());
                    } catch (MessagingException e) {
                        log.error(e.getMessage());
                    }
                });
            }

            // set attachments
            if (attachments != null) {
                attachments.entrySet().stream().forEach(element -> {
                    try {
                        msgHelper.addAttachment(element.getKey(), element.getValue());
                    } catch (MessagingException e) {
                        log.error(e.getMessage());
                    }
                });
            }

            log.debug("Begin to sendMail...");
            sender.send(message);
            result.setStatus(BaseResult.STATUS.SUCCESS);
            log.debug("SendMail success");
        } catch (Exception e) {
            log.error(e.getMessage());
            result.setStatus(BaseResult.STATUS.INTERNAL_ERROR);
            result.setMessage(e.getMessage());
        }
        return result;
    }

    private boolean isValidEmailAddress(String emailAddress) throws AddressException {
        if (!Util.isEmptyOrSpace(emailAddress) && emailAddress.matches(EMAIL_REGULAR_EXP))
            return true;
        else
            throw new AddressException("Invalid email addr: " + emailAddress);
    }

    private String getContentFromTemplate(String template, Map<String, Object> model) {
        StringBuffer content = new StringBuffer();
        try {
            content.append(FreeMarkerTemplateUtils
                    .processTemplateIntoString(fmConfiguration.getTemplate(template), model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}
