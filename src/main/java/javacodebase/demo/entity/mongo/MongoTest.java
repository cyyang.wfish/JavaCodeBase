package javacodebase.demo.entity.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "test")
public class MongoTest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String msg;

}