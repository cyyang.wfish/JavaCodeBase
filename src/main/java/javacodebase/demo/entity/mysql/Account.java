package javacodebase.demo.entity.mysql;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String email;
    private String nickname;
    private String name;
    private String phone;
    private String status;  // "A": Active, "D": Deactivate, "X": Deleted

    @JsonIgnore
    @Column(name = "active_code")
    private String activeCode;

    @JsonIgnore
    private String password;

    @Column(name = "reset_count")
    private int resetCount;

    @JsonIgnore
    @Column(name = "reset_code")
    private String resetCode;

}
