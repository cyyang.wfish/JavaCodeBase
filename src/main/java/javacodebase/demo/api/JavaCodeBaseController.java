package javacodebase.demo.api;

import javacodebase.demo.criteria.EmailCriteria;
import javacodebase.demo.criteria.KafkaConsumerCriteria;
import javacodebase.demo.criteria.KafkaProducerCriteria;
import javacodebase.demo.criteria.AccountCriteria;
import javacodebase.demo.kafka.KafkaRecord;
import javacodebase.demo.result.BaseResult;
import javacodebase.demo.service.AccountService;
import javacodebase.demo.service.EmailService;
import javacodebase.demo.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class JavaCodeBaseController {

    @Autowired
    private KafkaService kafkaService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AccountService accountService;

    @GetMapping(path = "/api/health", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> healthCheck() {
        return new ResponseEntity<String>("ok", HttpStatus.OK);
    }

    @PostMapping(path = "/api/kafka-producer", consumes = MediaType.APPLICATION_JSON_VALUE, produces =
            MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> kafkaProducer(@RequestBody KafkaProducerCriteria kafkaProducerCriteria) {
        List<KafkaRecord> records = new ArrayList<KafkaRecord>(kafkaProducerCriteria.getRecords());
        BaseResult result = kafkaService.producer(records);
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(path = "/api/kafka-consumer", consumes = MediaType.APPLICATION_JSON_VALUE, produces =
            MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> kafkaConsumer(@RequestBody KafkaConsumerCriteria kafkaConsumerCriteria) {
        BaseResult result = kafkaService.consumer(kafkaConsumerCriteria.getTopic());
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(path = "/api/send-email", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> sendEmail(@RequestBody EmailCriteria emailCriteria) {
        BaseResult result = emailService.sendEmail(emailCriteria, true, null, null);
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(path = "/api/account-info", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> getAccountInfo(@RequestParam(value = "account") String account) {
        BaseResult result = accountService.getAccountInfo(account);
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(path = "/api/signup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> signup(@RequestBody AccountCriteria accountCriteria) {
        BaseResult result = accountService.signup(accountCriteria);
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(path = "/api/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> login(@RequestBody AccountCriteria accountCriteria) {
        BaseResult result = accountService.login(accountCriteria);
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(path = "/api/activate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> activate(@RequestParam(value = "active_code") String activeCode) {
        BaseResult result = accountService.activate(activeCode);
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(path = "/api/forgot-pwd", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> forgotPwd(@RequestParam(value = "email") String email) {
        BaseResult result = accountService.forgotPwd(email);
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping(path = "/api/reset-pwd", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResult> resetPwd(@RequestBody AccountCriteria accountCriteria) {
        BaseResult result = accountService.resetPwd(accountCriteria);
        if (result.getStatus().equals(BaseResult.STATUS.SUCCESS))
            return new ResponseEntity<BaseResult>(result, HttpStatus.OK);
        else
            return new ResponseEntity<BaseResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
