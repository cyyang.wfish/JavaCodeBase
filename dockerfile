FROM openjdk:8-jre-alpine
LABEL description="Java Code Base API" \
      maintainer="cyyang.wfish@gmail.com"

ENV SETUP_ENV=uat
EXPOSE 8080

RUN mkdir -p /javacodebase
WORKDIR /javacodebase
ADD ./target/javacodebase-latest.jar /javacodebase/javacodebase-latest.jar
ENTRYPOINT "java" "-Dfile.encoding=utf-8" "-Dspring.profiles.active=$SETUP_ENV" "-jar" "/javacodebase/javacodebase-latest.jar"